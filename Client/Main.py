#!/usr/bin/python
# -*- coding: utf-8 -*-
import Client
import argparse


def get_args():
    """ Парсер аргументов командной строки """
    parser = argparse.ArgumentParser(description='Клиент для чата', prog='Client')
    parser.add_argument("ip", help="IP адрес сервера", type=str)
    parser.add_argument("port", help="Порт сервера", type=int)
    parser.add_argument("nick", help="Ник клиента", type=str)
    result = parser.parse_args()
    return result

if __name__ == "__main__":
    args = get_args()
    client = Client.Client(args.nick, args.ip, args.port)
    client.run()
    client.connect()
    client.keep_connection_alive()
