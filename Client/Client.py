# -*- coding: utf-8 -*-
import socket
import time
import threading


class Client:

    def __init__(self, name, server_address, port):
        self.name = name
        self.server_address = (server_address, port)
        self.buffer_size = 1024

        self.is_running = False
        self.is_connected = False
        self.is_still_connected = False

        self.receiving_thread = threading.Thread(target=self.recv)
        self.sending_thread = threading.Thread(target=self.send_input)
        self.sending_thread.daemon = True

        self.address = ""

        self.socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

    def run(self):
        self.socket.bind(("", 0))  # Забиндиться на свободный порт
        self.receiving_thread.start()
        self.sending_thread.start()
        self.is_running = True

    def recv(self):
        """ Пока не придёт подтверждение о регистрации, ждать его """
        while not self.is_running:
            time.sleep(0.1)
        while self.is_running:
            data, addr = self.socket.recvfrom(self.buffer_size)
            if addr == self.server_address:
                if not self.is_connected:
                    self._establish_connection(data)
                else:
                    self._process_message(data)

    def _establish_connection(self, message):
        if str(message).find("Вы зарегестрированы с адресом ") == 0:
            self.is_connected = True
            self.address = str(message)[str(message).rfind(' ') + 1:-1]
            print "Клиент подключен к серверу с адресом: " + self.server_address[0] \
                  + ":" + str(self.server_address[1]) + ".\r\nВаш адрес: " + self.address + "."

    def _process_message(self, message):
        """ Обработка сообщений: Если начинается с @ - обычное сообщение, иначе служебное"""
        if message[0] != "@":
            if message == "ping":
                self.is_still_connected = True
                self._send("pong")
            if message == "interrupt":
                return
        else:
            print message[1:]

    def send_input(self):
        while not self.is_running:
            time.sleep(0.1)
        while self.is_running:
            message = "@" + raw_input()
            if message == "@bye":
                self._exit()
            if self.is_connected:
                if len(message) > 1024:
                    print("Сообщение слишком длинное. Введите сообщение не длиннее 1023 символов")
                else:
                    self._send(message)

    def connect(self):
        """ Отправляется пакет вида register <nick> пока не придёт подтверждение"""
        print("Пытаемся подключиться к " + self.server_address[0] + ":" + str(self.server_address[1]))
        while not self.is_connected and self.is_running:
            self._send("register " + self.name)
            time.sleep(1)

    def keep_connection_alive(self):
        """Каждые 2 секунды проверяет, приходил ли пакет ping от сервера"""
        while self.is_connected and self.is_running:
            self.is_still_connected = False
            time.sleep(2)
            if self.is_running:
                if not self.is_still_connected:
                    print("Упс.. Похоже произошёл рызрыв соединения..")
                    self._exit()

    def _exit(self):
        self.is_running = False
        self._send_to_recv_thread("interrupt")

    def _send(self, msg):
        self.socket.sendto(msg, self.server_address)

    def _send_to_recv_thread(self, msg):
        ip, port = self.address.split(':')
        self.socket.sendto(msg, (ip, int(port)))
